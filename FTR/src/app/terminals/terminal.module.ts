import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TerminalRoutingModule } from "./terminal-routing.module";
import { TerminalhomeComponent } from "./terminalhome/terminalhome.component";
import { TerminalhomeService } from "./terminalhome/terminalhome.service";
import { TerminalsComponent } from "./terminals.component";
import { TerminaladdComponent } from './terminaladd/terminaladd.component';

@NgModule({
    imports: [
        CommonModule,
        TerminalRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [TerminalsComponent, TerminalhomeComponent, TerminaladdComponent],
    providers:[TerminalhomeService]
})

export class TerminalModule { }