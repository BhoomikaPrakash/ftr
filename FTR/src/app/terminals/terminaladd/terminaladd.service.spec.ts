import { TestBed } from '@angular/core/testing';

import { TerminaladdService } from './terminaladd.service';

describe('TerminaladdService', () => {
  let service: TerminaladdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerminaladdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
