import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import constants from 'src/assets/constants';
import { Terminal } from '../terminal';
import { TerminaladdService } from './terminaladd.service';

@Component({
  selector: 'app-terminaladd',
  templateUrl: './terminaladd.component.html',
  styleUrls: ['./terminaladd.component.css']
})
export class TerminaladdComponent implements OnInit {

  addTerminalHeader: String = constants.addTerminalHeader
  formDisplay: boolean =true
  addTerminalForm!: FormGroup
  terminal!:Terminal
  fieldRequired: String = constants.fieldRequired
  terminalNameError: String = constants.terminalNameError
  terminalDescriptionError: String = constants.terminalDescriptionError
  itemTypeError: String = constants.itemTypeError
  capacityError:String = constants.capacityError
  countryError:String = constants.countryError
  harborLocationError:String = constants.harborLocationError
  addTerminalSuccess: String = ""
  errorMessage: String = ""

  constructor(private formBuilder: FormBuilder, private activatedRoute:ActivatedRoute, private router:Router, private terminaladdService:TerminaladdService) { }

  ngOnInit(): void {
    this.addTerminalForm = this.formBuilder.group(
      {
        terminalName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
        country: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
        itemType: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
        terminalDescription: ["", [Validators.required, Validators.maxLength(25), Validators.pattern("^^(?=(?:[^-]*-){1}[^-]*$)(?=(?:\\D*\\d){1}\\D*$).*$")]],
        capacity: ["", [Validators.required, Validators.min(0), Validators.max(99999)]],
        status: ["", [Validators.required]],
        harborLocation: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(25)]],
        availableCapacity: ["", [Validators.required, Validators.min(0), Validators.max(99999)]]
      }
    )
  }

  addTeminal(){
    this.terminal = this.addTerminalForm.getRawValue()
    this.terminaladdService.addTeminal(this.terminal).subscribe({
      next: (data)=> {this.addTerminalSuccess=constants.addTerminalSuccess+data.terminalId, this.formDisplay=false},
      error: error=> {this.errorMessage = error.error.errorMessage, this.formDisplay=false}
    })
  }

}
