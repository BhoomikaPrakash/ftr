import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminaladdComponent } from './terminaladd.component';

describe('TerminaladdComponent', () => {
  let component: TerminaladdComponent;
  let fixture: ComponentFixture<TerminaladdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TerminaladdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TerminaladdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
