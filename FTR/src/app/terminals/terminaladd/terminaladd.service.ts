import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Terminal } from '../terminal';
import { catchError, Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TerminaladdService {

  constructor(private httpClient:HttpClient) { }
  
  private url = "http://localhost:9000/ftr/terminals"
  addTeminal(terminal:Terminal): Observable<Terminal>{
    const options = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post<Terminal>(this.url,terminal,{headers:options})
  }
}
