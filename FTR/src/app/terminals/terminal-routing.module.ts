import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TerminaladdComponent } from './terminaladd/terminaladd.component';
import { TerminalhomeComponent } from './terminalhome/terminalhome.component';


const terminalRoutes: Routes = [{
    path: '',
    children: [
      { path: 'home', component: TerminalhomeComponent },
      { path: 'addterminal', component: TerminaladdComponent },
      { path: '**', redirectTo: 'home', pathMatch: 'full' },

    ]
}
];
@NgModule({
    imports: [RouterModule.forChild(terminalRoutes)],
    exports: [RouterModule]
})
export class TerminalRoutingModule { }