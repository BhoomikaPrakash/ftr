import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalhomeComponent } from './terminalhome.component';

describe('TerminalhomeComponent', () => {
  let component: TerminalhomeComponent;
  let fixture: ComponentFixture<TerminalhomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TerminalhomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TerminalhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
