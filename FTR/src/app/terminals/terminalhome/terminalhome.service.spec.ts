import { TestBed } from '@angular/core/testing';

import { TerminalhomeService } from './terminalhome.service';

describe('TerminalhomeService', () => {
  let service: TerminalhomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerminalhomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
