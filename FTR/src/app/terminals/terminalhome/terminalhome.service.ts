import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import constants from 'src/assets/constants';
import { Terminal } from '../terminal';

@Injectable({
  providedIn: 'root'
})
export class TerminalhomeService {

  private url = "http://localhost:9000/ftr/terminals"

  constructor(private httpClient:HttpClient) { }


  viewterminals():Observable<Terminal[]>{
    return this.httpClient.get<Terminal[]>(this.url)
  }

  updateTerminalCapacity(terminalId:String, capacity:number):Observable<String>{
    const updateCapacityUrl = this.url+"/"+terminalId+"/"+capacity
    return this.httpClient.put(updateCapacityUrl,{},{responseType: 'text'})
  }
}
