import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import constants from 'src/assets/constants';
import { Terminal } from '../terminal';
import { TerminalhomeService } from './terminalhome.service';

@Component({
  selector: 'app-terminalhome',
  templateUrl: './terminalhome.component.html',
  styleUrls: ['./terminalhome.component.css']
})
export class TerminalhomeComponent implements OnInit {

  terminalArray!:Terminal[];
  noTerminals: String = "";
  terminalHeader: String = constants.terminalHeader;
  enableEdit = false;
  enableEditTeminal!: Terminal;
  enableEditIndex!:any;
  updateSuccess: String = "";
  updateFail: String = "";
  capacity!:number

  constructor(private modalService: NgbModal, private terminalhomeService:TerminalhomeService) { }

  ngOnInit(): void {
    this.viewTerminals()
  }

  viewTerminals(){
    this.terminalhomeService.viewterminals().subscribe({
      next:(data:Terminal[]) => {this.terminalArray = data},
      error: error => {this.noTerminals= error.error.errorMessage}}) 
  }

  enableEditMethod(terminal:Terminal, index:number){
    this.enableEdit = true,
    this.enableEditTeminal = terminal
    this.enableEditIndex = index
  }

  updateTerminalCapacity(terminal:Terminal, content:any){
    this.terminalhomeService.updateTerminalCapacity(terminal.terminalId, terminal.availableCapacity)
    .subscribe({
      next: data => {this.updateSuccess=data;this.updateFail=""; this.openModal(content)},
      error: error => {this.updateFail=JSON.parse(error.error).errorMessage;this.updateSuccess="";this.openModal(content);},
    })
  }

  openModal(content:any) {
    this.modalService.open(content);
  } 

  goBack(){
    this.modalService.dismissAll();
    this.enableEdit = false;
    this.enableEditIndex = -1
    this.ngOnInit()
  }
}
