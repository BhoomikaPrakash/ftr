import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import constants from 'src/assets/constants';
import { Terminal } from './terminal';
import { TerminalsService } from './terminals.service';

@Component({
  selector: 'app-terminals',
  templateUrl: './terminals.component.html',
  styleUrls: ['./terminals.component.css']
})
export class TerminalsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
