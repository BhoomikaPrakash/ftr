export class Terminal{
    terminalId!:String;
    terminalName!:String;
    country!:String;
    itemType!:String;
    terminalDescription!:String;
    capacity!:number;
    availableCapacity!:number;
    status!:String;
    harborLocation!:String;
}
