import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FTR';
  logout = false;
  aProfile = false;
  cProfile = false;
  constructor(private router: Router) {
    router.events.subscribe((val) => {
      this.logout = false;
      this.aProfile = false;
      this.cProfile = false;
      if (localStorage.getItem("role")=="admin") {
        this.logout = true;
        this.aProfile = true;
        this.cProfile = false;
      } else if (localStorage.getItem("role")=="customer") {
        this.logout = true;
        this.aProfile = false;
        this.cProfile = true;
      } else {
        this.aProfile = false;
        this.logout = false;
        this.cProfile = false;
      }
    });
  }

  logoutFn() {
    localStorage.clear()
    this.router.navigate(['/home'])
  }

}
