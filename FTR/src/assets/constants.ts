const constants = {
    noTerminals: `No Terminals Found. Please add one`,
    terminalHeader: `Play with Terminal`,
    addTerminalHeader: `Add Terminal`,
    fieldRequired: `field required`,
    terminalNameError: `Terminal Name should be minimum of 3 characters and maximum of 20 characters`,
    terminalDescriptionError: `Terminal Description is not in proper format - should be present`,
    itemTypeError: `Item type should be minimum of 4 characters and maximum of 30 characters`,
    capacityError: `Terminal available is maximum of size 99999`,
    harborLocationError: `Habor location should be minimum of 5 characters and maximum of 25 characters.`,
    countryError: `Country Name should be minimum of 3 characters and maximum of 20 characters.`,
    addTerminalSuccess: `Terminal added successfully with Id: `,
}

export default constants;